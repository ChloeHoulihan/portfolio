//
//  PortfolioApp.swift
//  Shared
//
//  Created by Chloe Houlihan on 18/05/2021.
//

import SwiftUI

@main
struct PortfolioApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
